<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Product;

class Category extends Model
{
    public $timestamps = false;
    protected $table = 'categories';
    
    protected $fillable = array(
        'name',
        'active'
    );

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'category';
    }

    public function products() {
        return $this->hasMany('App\Product', 'id_category');
    }
}
