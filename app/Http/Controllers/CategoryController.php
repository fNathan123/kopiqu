<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Http\Resources\DefaultResource;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $category = DB::table('categories')->get();
    return new DefaultResource($category);
  }

  public function listAll(Request $request)
  {
    return DefaultResource::collection(Category::with('product')->paginate(25));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request,[
      'name' => 'required',
    ]);
    
    $category = new Category([
        'name' => $request->name,
    ]);

    $category->save();

    return redirect('/admin/stock');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $category = DB::table('categories')->where('id', $id)->first();
    return response()->json($category);
  }

  public function get($id){
    $category = Category::with('products')->find($id);
    return new DefaultResource($category);
    //TODO: embed subproduct to product if possible
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $category = Category::find($id);
    dd($request->all());
    $category->update($request->only(['name']));
    return redirect('/admin/stock');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $category = Category::find($id);
    $category->delete();

    return redirect('/admin/stock');
  }
}
