<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class LoginController extends Controller
{
    //
    public function index(){
        return view('shop-page.login');
    }
    public function login(Request $request)
    {
        //dd($request->all());
        $user = DB::table('users')->where('email', $request->email)->where('password', $request->password)->get()->toArray();
        if(!empty($user)){
            //dd($user[0]->role);
            if(Session::has('login')){
                Session::forget('login');
            }
            $request->session()->put('login',$user[0]->role);
            if($user[0]->role==0){
                return redirect('admin/stock');
            }
            else if($user[0]->role==1){
                return redirect('/adminorder');
            }
        }
        else return redirect('/login');
    }

    public function logout(){
        Session::forget('login');
        return redirect('/login');
    }
}
