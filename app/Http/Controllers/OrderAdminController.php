<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Http\Resources\DefaultResource;
use Session;

class OrderAdminController extends Controller
{
    //
    public function index(Request $request)
    {
        if(Session::has('login') && Session::get('login')==1){
            $data = DB::table('orders')->get()->toArray();
            return view('admin-page.order',compact('data'));
        }
        else return redirect('/login');
        // return view('admin-page.order');
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $order = Order::find($id);
        //dd($product);
        $order->update($request->only(['status']));
        return redirect('/adminorder');
    }

    public function show($id)
    {
        // $problemset = Problemset::where('code', $code)->first();
        // return new DefaultResource($problemset);
        $product = DB::table('orders')->where('id', $id)->get();
        return new DefaultResource($product);
    }
}
