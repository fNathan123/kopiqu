<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Order_Item;
use Carbon\Carbon;
use Session;

class OrderController extends Controller
{
    public function create(){
        
        if(Session::has('cart')){
            $items=Session::get('cart');
            if($items->totalWeight<1000)return redirect('/cart')->with('failure','minimum weight is 1000 grams, please add more product');
            return view('shop-page.register');
        }
        return redirect('/cart')->with('failure','cart is empty');
    }

    public function store(Request $request){
        //dd($request->all());
        //dd($request->get('email'));
        if(Session::has('cart')){
            $items=Session::get('cart')->items;
            $totalPrice=Session::get('cart')->totalPrice;
            $mytime = Carbon::now();
            $bankTransfer=rand(100, 999);
            $shippingFee=Session::get('cart')->totalWeight/1000*5000;
            $payAmount=$totalPrice+$shippingFee-$bankTransfer;
            $request->session()->put('bankTransfer',$bankTransfer);
            $this->validate($request,[
                'email' => 'required',
                'nama'  => 'required',
                'alamat' => 'required',
                'kota' => 'required',
                'zip' => 'required',
                'phone' => 'required',
            ]);
            $order=new Order([
                'email' => $request->get('email'),
                'nama' => $request->get('nama'),
                'alamat' => $request->get('alamat'),
                'kota' => $request->get('kota'),
                'zip' => $request->get('zip'),
                'telephone' => $request->get('phone'),
                'totalPrice' => $payAmount,
                'purchased_at' => $mytime
            ]);
            $order->save();
            $maxid=DB::table('orders')->max('id');
            foreach($items as $key=>$value) {
                $order_item=new Order_Item([
                    'order_id'=>$maxid,
                    'product_id'=>$key
                ]);
                $order_item->save();
            }


        }
        return redirect('/after');

    }
}
