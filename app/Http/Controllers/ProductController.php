<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Cart;
use App\Http\Resources\DefaultResource;
use Session;


class ProductController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Session::has('login') && Session::get('login')==0){
            $products = DB::table('products')->get()->toArray();
            $category=DB::table('categories')->get()->toArray();
            $data=array($category,$products);
            return view('admin-page.stock',compact('data'));
        }
        else return redirect('/login');
    }

    public function getAddToCart(Request $request, $id ){
        $product=Product::find($id);
        $oldcart;
        if(Session::has('cart'))$oldcart=Session::get('cart');
        else $oldcart=null;
        $cart=new Cart($oldcart);
        $cart->add($product,$id);
        $request->session()->put('cart',$cart);
        //dd($request->session()->get('cart'));
        return redirect('/home');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if(Session::get('login')==0){
        $this->validate($request,[
            'name' => 'required',
            'id_category'  => 'required',
            'price' => 'required',
            'weight' => 'required',
            'photo_path'=>'required'
        ]);

        $product = new Product([
            'name' => $request->name,
            'id_category' => $request->id_category,
            'price' => $request->price,
            'weight' => $request->weight,
            'photo_path' => $request->photo_path
        ]);

        $product->save();

        return redirect('/admin/stock');
        }
        else return redirect('/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $problemset = Problemset::where('code', $code)->first();
        // return new DefaultResource($problemset);
        $product = DB::table('products')->where('id', $id)->get();
        return new DefaultResource($product);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $product = Product::find($id);
        //dd($product);
        $product->update($request->only(['name', 'price', 'weight','id_category','photo_path']));
        return redirect('/admin/stock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $product = Product::find($id);
        $product->delete();
        return redirect('/admin/stock');
    }

    public function clearcart(){
        if(Session::has('cart'))Session::forget('cart');
        return redirect('/cart');
    }
}
