<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Order_Item;


class Order extends Model
{
    public $timestamps = false;
    protected $fillable = array(
        'email',
        'nama',
        'alamat',
        'kota',
        'zip',
        'telephone',
        'totalPrice',
        'purchased_at',
        'status'
    );

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'order';
    }

    public function order_items() {
        return $this->hasMany('App\Order_Item', 'order_id');
    }
}
