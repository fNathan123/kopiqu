<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Order;
use App\Product;


class Order_Item extends Model
{
    public $timestamps = false;
    protected $table = 'order_items';
    protected $fillable = array(
        'order_id',
        'product_id',
    );

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'order_item';
    }

    
    public function order_items() {
        return $this->belongsTo('App\Order_Item');
    }

    public function products(){
        return $this->hasOne('App\Product','id');
    }

}
