<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Category;

class Product extends Model
{
    public $timestamps = false;
    protected $table = 'products';
    protected $fillable = array(
        'name',
        'id_category',
        'price',
        'weight',
        'photo_path',
        'active'
    );

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'product';
    }

    public function products() {
        return $this->belongsTo('App\Category', 'id_category');
    }
}
