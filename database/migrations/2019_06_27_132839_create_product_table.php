<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256)->nullable(false);
            $table->unsignedInteger('id_category')->nullable(false);
            $table->double('price')->nullable(false);
            $table->double('weight')->nullable(false);
            $table->string('photo_path',256);
            $table->tinyInteger('active_status')->default(0);
        });

        Schema::table('products', function(Blueprint $table){
            $table->foreign('id_category')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
