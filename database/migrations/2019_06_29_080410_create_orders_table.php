<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',256)->nullable(false);
            $table->string('nama',256)->nullable(false);
            $table->string('alamat',256)->nullable(false);            
            $table->string('kota',50)->nullable(false);
            $table->string('zip',50)->nullable(false);
            $table->string('telephone',256)->nullable(false);
            $table->double('totalPrice')->nullable(false);
            $table->datetime('purchased_at')->nullable(false);
            $table->tinyInteger('status')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
