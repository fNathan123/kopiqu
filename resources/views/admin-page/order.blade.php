@extends('master')

@section('content')
<div style="min-height:90vh">
    <div class="container" style="min-height:90vh;">
    <h3 class="m-3 p-2">Order admin page</h3>
    <?php
        //print_r($data[0]);
    ?>
    <div class="row">
    <div class="col-sm-12">
        <div class="row mt-3 p-2">
            <table class="table table-striped table-bordered p-2" id="example">
                <thead class="thead-dark">
                <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>Zip</th>
                    <th>Telephone</th>
                    <th>TotalPrice</th>
                    <th>Purchased At</th>
                    <th>Status</th> 
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $idx=1;?>
                @foreach($data as $key=>$row)
                <tr>
                    <td>{{$idx}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->nama}}</td>
                    <td>{{$row->alamat}}</td>
                    <td>{{$row->kota}}</td>
                    <td>{{$row->zip}}</td>
                    <td>{{$row->telephone}}</td>
                    <td>{{$row->totalPrice}}</td>
                    <td>{{$row->purchased_at}}</td>
                    <td>{{$row->status}}</td>
                    <td>
                    <button class="d-inline btn btn-primary mt-3" value="{{$key}}" id="editBut{{$key}}" data-toggle="modal" data-target="#formModal" onclick="openModal('{{$row->id}}')">Edit</button>
                    </td>

                </tr>
                <?php $idx++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
</div>

</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="orderForm" action="{{url('/adminorder')}}" method="post">
      {{csrf_field()}}
      <input type="hidden" value="PATCH" name="_method" id="hidIn" />
      <select class="custom-select my-1 mr-sm-2" name="status">
            <option value="0">0 - Pending</option>
            <option value="1">1 - Paid</option>
            <option value="2">2 - Shipped</option>
    </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>

@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready( function () {
    $('#example').DataTable();
} );

function openModal (id) {
    console.log("msk");
    var link="/adminorder/"+id;
    console.log(link);
    $.ajax({
        type: "GET",
        data: "",
        url: link,
        dataType: 'json',
        success: function(data){
        }
        }).done(function(data){
            //console.log(data);
            //$('#nameIn').val(data.data[0].name);
            var act="/adminorder/"+data.data[0].id;
            console.log(act);
            //var act=data.data[0].id;
            $('#orderForm').attr('action',act);
            $('#hidIn').attr('value','PATCH');
        });
};

</script>