@extends('master')

@section('content')
<div style="min-height:90vh">
    <div class="container" style="min-height:90vh;">
    <h3 class="m-3 p-2">Stock admin page</h3>
    <?php
        //print_r($data[0]);
    ?>
    <div class="row">
    <div class="col-sm-12">
      @if(count($errors)>0)
      <div class="alert alert-danger">
          <ul>
          @foreach($errors->all() as $error)
              <li>{{$error}}</li>
          @endforeach
          </ul>
      </div>
      @endif
        <button class="btn btn-primary mt-3" data-toggle="modal" data-target="#formModal">Add Product</button>
        <div class="row mt-3 p-2">
            <table class="table table-striped table-bordered p-2" id="example">
                <thead class="thead-dark">
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Category</th>
                    <th>Photo Path</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $idx=1;?>
                @foreach($data[1] as $key=>$row)
                <tr>
                    <td>{{$idx}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->price}}</td>
                    <td>{{$row->weight}}</td>
                    <td>{{$row->id_category}}</td>
                    <td>{{$row->photo_path}}</td>
                    <td>
                    <button class="d-inline btn btn-primary mt-3" value="{{$key}}" id="editBut{{$key}}" data-toggle="modal" data-target="#formModal" onclick="openModal('{{$row->id}}')">Edit</button>
                    <form class="d-inline"method="post" action="{{$row->id}}">
                    {{csrf_field()}}
                    <input type="hidden" value="DELETE" name="_method" id="hidDel" />
                    <button class="d-inline btn btn-danger mt-3" value="{{$row->id}}" id="deleteBut{{$row->id}}" onclick="deleterow()">Delete</button>
                    </form>
                    <button class="btn btn-primary mt-3" data-toggle="modal" data-target="#uploadModal">Upload Photo</button>
                    </td>

                </tr>
                <?php $idx++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
    <div class="col-sm-12">
        <button class="btn btn-primary mt-3" data-toggle="modal" data-target="#cateModal">Add Categories</button>
        <div class="row mt-3 p-2">
            <table class="table table-striped table-bordered p-2" id="example2">
                <thead class="thead-dark">
                <tr>
                    <th>No</th>
                    <th>Nama Category</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $idx=1;?>
                @foreach($data[0] as $key=>$row)
                <tr>
                    <td>{{$idx}}</td>
                    <td>{{$row->name}}</td>
                    <td>
                    <form class="d-inline"method="post" action="{{action('CategoryController@destroy',$row->id)}}">
                    {{csrf_field()}}
                    <input type="hidden" value="DELETE" name="_method" id="hidDel" />
                    <button class="d-inline btn btn-danger mt-3" value="{{$row->id}}" id="deleteBut2{{$row->id}}" onclick="deleterow()">Delete</button>
                    </form>
                    </td>

                </tr>
                <?php $idx++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>
</div>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="productForm" action="{{url('/admin/')}}" method="post">
      {{csrf_field()}}
        <input type="hidden" value="" name="_method" id="hidIn" />
        <div class="form-group" >
            <label for="name">Nama Produk</label>
            <input type="text" class="form-control" id="nameIn" name="name" aria-describedby="" placeholder="Nama Produk">
        </div>
        <div class="form-group">
            <label for="harga">Harga</label>
            <input type="number" class="form-control" id="priceIn" name="price" aria-describedby="" placeholder="Harga">
        </div>
        <div class="form-group">
            <label for="weight">Berat</label>
            <input type="number" class="form-control" id="weightIn" name="weight" aria-describedby="" placeholder="Berat">
        </div>
        <div class="form-group">
        <label class="my-1 mr-2" for="id_category">Category</label>
        <select class="custom-select my-1 mr-sm-2" name="id_category">
            @foreach($data[0] as $key=>$row)
            <option value="{{$row->id}}">{{$row->id}} - {{$row->name}}</option>
            @endforeach
        </select>
        <div class="form-group">
            <label for="photo_path">Path</label>
            <input type="text" class="form-control" id="photo_path" name="photo_path" aria-describedby="" placeholder="path">
        </div>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control">
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="cateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="categoryForm" action="{{url('/category/')}}" method="post">
      {{csrf_field()}}
        <input type="hidden" value="" name="_method" id="hidInCate" />
        <div class="form-group" >
            <label for="name">Nama Category</label>
            <input type="text" class="form-control" id="cateIn" name="name" aria-describedby="" placeholder="Nama Produk">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>


@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready( function () {
    $('#example').DataTable();
} );
$(document).ready( function () {
    $('#example2').DataTable();
} );

function openModal (id) {
    console.log("msk");
    var link="/admin/"+id;
    console.log(link);
    $.ajax({
        type: "GET",
        data: "",
        url: link,
        dataType: 'json',
        success: function(data){
            //console.log(data);
            //data will contain the vote count echoed by the controller i.e.  
                //"yourVoteCount"
            //then append the result where ever you want like
            //$("span#votes_number").html(data); //data will be containing the vote count which you have echoed from the controller
        }
        }).done(function(data){
            //console.log(data);
            $('#nameIn').val(data.data[0].name);
            $('#priceIn').val(data.data[0].price);
            $('#weightIn').val(data.data[0].weight);
            $('#photo_path').val(data.data[0].photo_path);
            //var act="action('ProductController@update',"+data.data[0].id+")";
            var act=data.data[0].id;
            $('#productForm').attr('action',act);
            $('#hidIn').attr('value','PATCH');
        });
};


function deleterow () {
    if(confirm("Are you sure want to delete it?")){
        return true;
    }
    else return false;
;}
</script>