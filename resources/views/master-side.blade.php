@extends('master')
@section('content')
  <!-- Page Content -->
  <div class="container" style="min-height:90vh;">

    <div class="row">

      <div class="col-lg-3">

        <h1 class="my-4">Kopiqu Store</h1>
        <div class="list-group">
          <h4 class="mt-3 list-group-item">Categories</h4>
          @foreach($data[0] as $row)
          <a href="{{ url('/search/' . $row->id) }}" class="list-group-item">{{$row->name}}</a>
          @endforeach
        </div>

      </div>
      <!-- /.col-lg-3 -->
       @yield('prod_content')
      

    </div>
    <!-- /.row -->

  </div>
@endsection