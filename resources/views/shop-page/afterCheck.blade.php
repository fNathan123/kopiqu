@extends('master')
@section('content')
<div style="min-height:90vh">
    <div class="container" style="min-height:90vh;">
    <div class="row">
    
    <div class="col-sm-12 mt-4 p-3 border">
        <h2>Your Transfer Bill</h2>
        <span>
        Check Your E-mail or phone for further notification and dont hesitate to contact us if you are having problems.<br>
        We will sent you email about update your shipment status.<br>
        Currently, we only accept payment via bank transfer. Your Payment valid for next 24 hours.
        </span>
        <br><br>
        <table class="table table-borderless">
            <tr>
                <td>Products </td>
                <td> : </td>
                <td> Rp.<?php 
                    if(Session::has('cart')){
                        $total=Session::get('cart')->totalPrice; 
                        echo $total;
                    }?>,00
                    </td>
            </tr>
            <tr>
                <td>Shipping Fee </td>
                <td> : </td>
                <td> Rp.<?php 
                        if(Session::has('cart')){
                            $shipping=Session::get('cart')->totalWeight/1000*5000;
                            echo $shipping;
                        }?>,00</td>
            </tr>
            <tr>
                <td>Bank Transfer </td>
                <td> : </td>
                <td> Rp. -<?php 
                    if(Session::has('bankTransfer')){
                        $bank=Session::get('bankTransfer');
                        echo $bank;
                    }?>,00</td>
            </tr>
            <tr>
                <td>Total </td>
                <td> : </td>
                <td>Rp. <?php 
                        echo $total+$shipping-$bank;
                        Session::forget('bankTransfer');
                        Session::forget('cart');
                        ?>,00 </td>
            </tr>
        </table>
        <a href="{{url('/home')}}" class="btn btn-primary mt-3">back to shop</a>
    </div>
    </div>
    </div>
</div>
@endsection