@extends('master')

@section('content')
<div style="min-height:90vh">
    <div class="container" style="min-height:90vh;">
    <h3 class="m-3 p-2">Shopping Cart</h3>

    <div class="row">
    
    <div class="col-sm-12 border">
        <div class="row mt-3 p-2">
            <div class="col-sm-9">
            @if(\Session::has('failure'))
            <div class="alert alert-danger">
                <p>{{\Session::get('failure')}}</p>
            </div>
            @endif
                <table class="table">
                    <tr>
                        <th scope="col"></td>
                        <th scope="col">Product Name</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Qty</th>
                        <th scope="col">SubTotal</th>
                    </tr>
                    <?php
                        $cartTemp=Array();
                        $sessionTemp;
                        if(Session::has('cart')){
                            $cartTemp=Session::get('cart')->items;
                            $sessionTemp=Session::get('cart');
                        }
                        //print_r($sessionTemp->totalWeight);
                    ?>
                    @foreach($cartTemp as $row)
                    <tr>
                        <td>
                        <div class="card" style="width: 14rem;">
                            <img class="card-img-top" src="http://placehold.it/700x400" alt="Card image cap">
                        </div>
                        </td>
                        <td>{{$row['item']->name}}</td>
                        <td>Rp.{{($row['price']/$row['qty'])}},00</td>
                        <td>{{$row['qty']}}</td>
                        <td>Rp.{{$row['price']}},00</td>
                    </tr>
                    @endforeach

                
                </table>
                <hr>
                <a href="/clearcart" class="p-1 m-3 btn btn-primary">Clear Cart</a>
            </div>

            <div class="col-sm-3">
                <div class="card">
                <div class="card-header">
                    <b>Cart Totals</b>
                </div>
                <div class="card-body">
                    <table class="m-3 p-3">
                        <tr>
                            <td>Total</td>
                            <td> : </td>
                            <td> 
                                <?php 
                                if(isset($sessionTemp))echo "Rp.".$sessionTemp->totalPrice.",00";
                                else echo "Rp.0,00";
                                ?> </td>
                        </tr>
                        <tr>
                            <td>Total Weight</td>
                            <td> : </td>
                            <td> 
                                <?php 
                                if(isset($sessionTemp))echo $sessionTemp->totalWeight." grams";
                                else echo "0 grams";
                                ?> 
                            </td>
                        </tr>
                    </table>
                    <a href="/regis/create" class="btn btn-primary">Proceed To Checkout</a>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
@endsection