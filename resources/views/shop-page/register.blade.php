@extends('master')
@section('content')

<div style="min-height:90vh">
<div class="container p-3 mt-4">
    <div class="row">
    <div class="col-sm-9 mx-auto">
    @if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <h3 class="m-3 p-1">Data Form</h3>
    <form method="post" action="{{url('/regis')}}">
    @csrf
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="nama" placeholder="Nama mu">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" name="phone" placeholder="0xxxxxxx">
    </div>
    <div class="form-row">
    <div class="form-group col-md-6">
        <label for="address">Address</label>
        <input type="text" class="form-control" name="alamat" placeholder="Jl. alamatmu">
    </div>
    <div class="form-group col-md-3">
        <label for="city">City</label>
        <input type="text" class="form-control" name="kota" placeholder="Kota mu">
    </div>
    <div class="form-group col-md-3">
        <label for="zip">Zip</label>
        <input type="text" class="form-control" name="zip" placeholder="kode pos mu">
    </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    </div>
</div>
</div>

@endsection