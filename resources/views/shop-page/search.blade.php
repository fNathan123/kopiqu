@extends('master-side')

@section('prod_content')
      <div class="col-lg-9 mt-4 pb-5 mb-3">
      <h2 class="mb-3">Our Products</h2>
        <div class="row">

          @foreach($data[1] as $row)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#">{{$row->name}}</a>
                </h4>
                <h5>Rp.{{$row->price}},00</h5>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
              </div>
              <div class="card-footer">
                <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
              </div>
            </div>
          </div>
          @endforeach

        </div>
        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->
@endsection
