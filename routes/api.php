<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['prefix' => 'v1'], function () {
//     Route::apiResources([
//         'category' => 'CategoryController',
//         'product' => 'ProductController',
//     ]);

//     Route::get('/welcome', function () {
//         return view('welcome');
//     });

//     Route::group(['prefix' => 'findAll'], function(){
//         Route::get('/category', 'CategoryController@index');
//         Route::get('/product', 'ProductController@listAll');
//     });

//     Route::group(['prefix' => 'find'], function(){
//         Route::get('/category/{id}', 'CategoryController@get');
//     });
// });