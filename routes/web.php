<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cart', function () {
    return view('shop-page/cart');
});
Route::get('/after', function () {
    return view('shop-page/afterCheck');
});

// Route::get('/admin/stock', function () {
//     return view('admin-page/stock');
// });

Route::get('/register', function () {
    return view('shop-page/register');
});

Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');

Route::get('/add-to-cart/{id}',
    ['uses'=>'ProductController@getAddToCart',
     'as'=>'product.addToCart']);

Route::get('/admin/stock',
    ['uses'=>'ProductController@index',
    'as'=>'admin.stock']);

Route::post('/login',
    ['uses'=>'LoginController@login',
    'as'=>'login.user']);
Route::get('/logout',
    ['uses'=>'LoginController@logout',
    'as'=>'logout.user']);

Route::get('/login',
    ['uses'=>'LoginController@index',
    'as'=>'login.page']);
Route::get('/clearcart',
    ['uses'=>'ProductController@clearcart',
    'as'=>'clear.cart']);

// Route::get('/adminorder',
//     ['uses'=>'OrderAdminController@index',
//     'as'=>'admin.order']);


Route::resource('category', 'CategoryController');
Route::resource('home','SearchController');
Route::resource('search','SearchController');
Route::resource('regis','OrderController');
Route::resource('admin','ProductController');
Route::resource('adminorder','OrderAdminController');